using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class counterBall
{
    public static counterBall counter = new counterBall();
    int balls = 5;

    public Action<int> counterChange;
    
    public int numBall
    {
        get { return balls; } 
        set 
        { 
           balls = value; 
           counterChange?.Invoke(numBall);
        }
    }
    public void addBall()
    {
        balls++;
        counterChange?.Invoke(numBall);
    }

    public void removeBall()
    {
        balls--;
        counterChange?.Invoke(numBall);
    }
}
