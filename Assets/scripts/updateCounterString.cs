using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class updateCounterString : MonoBehaviour
{
    TMPro.TMP_Text txt;
    // Start is called before the first frame update
    void Start()
    {
        txt = GetComponent<TMPro.TMP_Text>();
        counterBall.counter.counterChange += UpdateText;
        UpdateText(counterBall.counter.numBall);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void UpdateText(int num)
    {
        txt.text = "x" + num.ToString();

    }
}
