using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
    public GameObject ball;
    public float offset = 1.0f;
    public float fireRate = 1.0f;
    float lastFire = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        lastFire = fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        lastFire += Time.deltaTime;
        if(counterBall.counter.numBall > 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if(lastFire > fireRate)
                {
                    lastFire %= fireRate;
                    GameObject oBall =  Instantiate(ball);
                    oBall.transform.position = transform.position - transform.up * offset;
                    counterBall.counter.removeBall();
                    print(oBall.GetComponent<moveBall>());
                    oBall.GetComponent<Rigidbody2D>().AddForce(-transform.up * 9.81f, ForceMode2D.Impulse);
                }
            }

        }
    }
}
